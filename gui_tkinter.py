from tkinter import *
from tkinter import filedialog
import detect_cars_cascade as dcc

class GUI:
  def __init__(self, root):
    self._filename = ""
    self.root = root
    self.root.title = "Autok detektalasa"
    self.Browse = Button(self.root, text="Tallózás...",
                          command=self.browse_button)
    self.Browse.grid(row=0, column=0, columnspan=5, sticky=W+E)
    self.cascade_real = Button(self.root, text="Cascade módszer real-time futtatása",
                          command=lambda *args: dcc.detect_cars(self._filename))
    self.cascade_real.grid(row=1, column=0,columnspan=5,sticky=W+E)

    self.cascade_kiir = Button(self.root, text="Cascade módszer kiírása",
                          command=lambda *args: dcc.write_video(self._filename))
    self.cascade_kiir.grid(row=2, column=0,columnspan=5, sticky=W+E)

    self.watch_video = Button(self.root, text="Beolvasott videó megtekintése",
                          command=lambda *args: dcc.watch_video(self._filename))
    self.watch_video.grid(row=3, column=0,columnspan=5, sticky=W+E)

  def browse_button(self):
    self._filename = filedialog.askopenfilename()
    print(self._filename)

if __name__ == '__main__':
  root = Tk()
  GUI(root)
  root.mainloop()
