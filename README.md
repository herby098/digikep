Digitális képfeldolgozás haladóknak kötprog

Tallózás gombbal ki lehet választani a kívánt videót. Ezután a 'Cascade módszer real-time futtatása' gombbal valós időben
tudjuk nézni a felismerés folyamatát. Cascade módszer kiírásával a felismerést egy avi kiterjesztésű fájlba írjuk. A kiírt
videót a 'Beolvasott videó megtekintése' gombbal tudjuk megnézni. A megnyitott videót mindegyik gomb esetében az escape
leütésével lehet bezárni.