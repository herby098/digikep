import cv2
import numpy as np
import time


def write_video(input_video_path):
  t0 = time.time()
  res = None
  out = None
  cascade_src = 'cars.xml'
  video_src = input_video_path
  car_cascade = cv2.CascadeClassifier(cascade_src)
  cap = cv2.VideoCapture(video_src)


  frame_width = int(cap.get(3))
  frame_height = int(cap.get(4))
  fourcc = cv2.VideoWriter_fourcc(*'MJPG')
  out = cv2.VideoWriter(f"{input_video_path.split('.')[0]}_cascade.avi",
                        fourcc, 20.0, (frame_width,frame_height))
  while True:
    ret, img = cap.read()
    if ret is not True:
      break
    if (type(img) == type(None)):
      break
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    height, width = gray.shape
    sub_img_top = img[0:height//2, :]
    sub_img_bot = img[height//2:height, :]
    sub_img = gray[height // 2:height, :]
    cars = car_cascade.detectMultiScale(sub_img, 1.1, 2)
    cv2.putText(sub_img_top, f'Autok szama: {len(cars)}', (0, 20), cv2.FONT_HERSHEY_DUPLEX, 1, (255, 0, 0), 1)
    for (x, y, w, h) in cars:
      cv2.rectangle(sub_img_bot, (x, y), (x + w, y + h), (255, 0, 0), 2)
      cv2.putText(sub_img_bot, 'Auto', (x + 6, y - 6), cv2.FONT_HERSHEY_DUPLEX, 0.5, (255, 0, 0), 1)

    res = np.concatenate((sub_img_top, sub_img_bot), axis=0)
    cv2.line(res, (0, height//2), (width, height//2), (255,0,0), 2)
    #cv2.imshow("video", res)
    out.write(res)
    if cv2.waitKey(33) == 27:
      break
  cap.release()
  out.release()
  cv2.destroyAllWindows()
  t1 = time.time()
  print(f"Time {t1-t0}")


def detect_cars(input_video_path):
  res = None
  cascade_src = 'cars.xml'
  video_src = input_video_path
  car_cascade = cv2.CascadeClassifier(cascade_src)
  cap = cv2.VideoCapture(video_src)

  while True:
    ret, img = cap.read()
    if ret is not True:
      break
    if (type(img) == type(None)):
      break
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    height, width = gray.shape
    sub_img_top = img[0:height // 2, :]
    sub_img_bot = img[height // 2:height, :]
    sub_img = gray[height // 2:height, :]
    cars = car_cascade.detectMultiScale(sub_img, 1.1, 2)
    cv2.putText(sub_img_top, f'Autok szama: {len(cars)}',(0,20), cv2.FONT_HERSHEY_DUPLEX, 1, (255,0,0), 1)
    for (x, y, w, h) in cars:
      cv2.rectangle(sub_img_bot, (x, y), (x + w, y + h), (255, 0, 0), 2)
      cv2.putText(sub_img_bot, 'Auto', (x + 6, y - 6), cv2.FONT_HERSHEY_DUPLEX, 0.5, (255, 0, 0), 1)
    res = np.concatenate((sub_img_top, sub_img_bot), axis=0)
    cv2.line(res, (0, height // 2), (width, height // 2), (255, 0, 0), 2)

    cv2.imshow("video", res)

    if cv2.waitKey(33) == 27:
      break
  cap.release()
  cv2.destroyAllWindows()


def watch_video(input_video_path):
  video_src = input_video_path
  cap = cv2.VideoCapture(video_src)
  while True:
    ret, img = cap.read()
    if ret is not True:
      break
    if (type(img) == type(None)):
      break
    cv2.imshow('video', img)
    if cv2.waitKey(33) == 27:
      break
  cap.release()
  cv2.destroyAllWindows()
